module gitlab.com/dylan-evans/gogo

go 1.16

require (
	github.com/faiface/pixel v0.10.0 // indirect
	gopkg.in/teh-cmc/go-sfml.v24 v24.0.0-20170910220153-372cfe28f3e7 // indirect
)
